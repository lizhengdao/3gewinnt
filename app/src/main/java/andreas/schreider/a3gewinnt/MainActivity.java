package andreas.schreider.a3gewinnt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import androidx.gridlayout.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    Vibrator vibe;
    //GAME
    int winningLength = 3;
    boolean gameOver = false;
    int rows = 3;
    int columns = 3;
    //0=neutral; 1 = Player1; 2= Player2
    int[] gameState = new int[rows * columns];
    int currentPlayer = 1;

    //INFOS
    String playerName1 = "CIRCLE";
    String playerName2 = "CROSS";
    String runningGameMSG = "It's your turn!";
    String winMSG = "Congrats";

    //VIEW
    TextView tvPlayerName;
    TextView tvStatus;
    ImageView ivLeftCrown;
    ImageView ivRightCrown;
    
    /*
    INFOS ABOUT GAME
     */
    public void updateInfo (String smallText, String largeText){
        tvStatus.setText(smallText);
        tvPlayerName.setText(largeText);
    }


    public void enableCrown (){
            ivLeftCrown.setAlpha(1.f);
            ivRightCrown.setAlpha(1.f);
    }

    public void disableCrown (){
        ivLeftCrown.setAlpha(0.f);
        ivRightCrown.setAlpha(0.f);
    }

    /*
    GAME LOGIC
     */
    public void resetGame (){
        vibe.vibrate(80);
        //clear view and gamestate
        GridLayout field = findViewById(R.id.field);
        for (int i = 0; i < field.getChildCount(); i++) {
            gameState[i] = 0;
            View subView = field.getChildAt(i);

            if (subView instanceof ImageView) {
                ImageView imageView = (ImageView) subView;
                imageView.setImageResource(0);
            }
        }
        disableCrown();
        gameOver = false;

        //reset information
        tvStatus.setText(runningGameMSG);
        if (currentPlayer == 1){
            tvPlayerName.setText(playerName1);
        }else {
            tvPlayerName.setText(playerName2);
        }
        Log.i("Gamestate:", Arrays.toString(gameState));
    }

    public void makeTurn(ImageView ivPosition){
        if ((gameState[Integer.parseInt(ivPosition.getTag().toString())] == 0) && !gameOver) {
            vibe.vibrate(50);
            if (currentPlayer == 1) {
                ivPosition.setImageResource(R.drawable.circle2);
                tvPlayerName.setText(playerName2);
                gameState[Integer.parseInt(ivPosition.getTag().toString())] = currentPlayer;

                if (isGameOver()) {
                    updateInfo(winMSG, playerName1);
                    enableCrown();
                }
                currentPlayer = 2;
            } else if (currentPlayer == 2) {
                ivPosition.setImageResource(R.drawable.cross2);
                tvPlayerName.setText(playerName1);
                gameState[Integer.parseInt(ivPosition.getTag().toString())] = currentPlayer;

                if (isGameOver()) {
                    updateInfo(winMSG, playerName2);
                    enableCrown();
                }
                currentPlayer = 1;
            }
            Log.i("Gamestate:", Arrays.toString(gameState));
        }
    }

    public boolean isGameOver() {
        if(isRowComplete(winningLength, rows, columns)
                || isColComplete(winningLength, rows, columns)
                || isLeftDiagonalComplete(winningLength,rows, columns)
                || isRightDiagonalComplete(winningLength, rows, columns)){
            gameOver = true;
            return true;
        }else{
            return false;
        }
    }

    public boolean isColComplete(int winningLength, int rows, int columns) {
        //check columns
        int sequence = 0;

        for (int i = 0; i < (rows * columns); i += columns) {
            sequence = 0;
            for (int j = 0; j < rows; j++) {
                if ((gameState[i + j] == currentPlayer)) {
                    sequence++;
                } else {
                    continue;
                }
                if (sequence == winningLength) {
                    Log.i("Winning Type:", "column");
                    return true;

                }
            }
        }
        return false;
    }

    public boolean isRowComplete(int winningLength, int rows, int columns) {
        //check rows
        int sequence = 0;
        for (int i = 0; i < columns; i++){
            for (int j = 0; j <rows; j++)
                if ((gameState[i + (j * columns) ] == currentPlayer)){
                    sequence ++;
                }else{
                    sequence = 0;
                    continue;
                }
            if (sequence == winningLength){
                Log.i("Winning Type:", "rows");
                return true;
            }


        }
        return false;
    }

    public boolean isLeftDiagonalComplete(int winningLength, int rows,  int columns) {
        //check diagonale left to right
        int sequence = 0;

        for (int j = 0; j <rows; j++) {
            if ((gameState[(j * columns) + j] == currentPlayer)) {
                sequence++;
            } else {
                sequence = 0;
                continue;
            }
            if (sequence == winningLength) {
                Log.i("Winning Type:", "l to r diagnole");
                return true;
            }
        }
        return false;
    }

    public boolean isRightDiagonalComplete(int winningLength,int rows, int columns) {
        int sequence = 0;
        //check diagonale right to left
        for (int j = 0; j < rows; j++) {
            if ((gameState[((columns - 1) + ((columns - 1) * j))] == currentPlayer)) {
                sequence++;
            } else {
                sequence = 0;
                continue;
            }
            if (sequence == winningLength) {
                Log.i("Winning Type:", "r to l diagnole");
                return true;
            }
        }
        return false;
    }

    /*
    EVENTS
     */
    public void setIcon(View view){
        makeTurn((ImageView) view);
    }

    public void newGame(View view){
        resetGame();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vibe = (Vibrator) MainActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
        tvPlayerName = (TextView) findViewById(R.id.tvPlayerName);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        ivLeftCrown = (ImageView) findViewById(R.id.ivWin);
        ivRightCrown = (ImageView) findViewById(R.id.ivWin2);
        tvPlayerName.setText(playerName1);
    }
}
